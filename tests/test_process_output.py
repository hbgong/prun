import unittest
from unittest.mock import patch

import logging
import csv
from pathlib import Path
from tempfile import TemporaryDirectory


# Suppress the CryptographyDeprecationWarning from Paramiko
import warnings
warnings.simplefilter("ignore")

from prunlib.session import Session
from process_output import process_err_devlist, process_ok_devlist

# logging.basicConfig(level=logging.DEBUG)


class TestProcessDevList(unittest.TestCase):
    """Test process_*_devlist fuctions in the process_output module."""

    def setUp(self):
        count = 4
        self.ssn_list = [
            Session(f'10.1.1.{i}', f'dev-{i}', 'adm', 'pwd')
            for i in range(1, count)
        ]

        self.base = TemporaryDirectory(dir='./tests', delete=False) # no auto-cleanup
        self.job_folder = Path(self.base.name)
        # print(self.job_folder.absolute())


    def tearDown(self):
        self.base.cleanup()


    def test_process_err_devlist(self):
        for s in self.ssn_list:
            s.errmsg = f'error msg {s.hostname}'

        r = process_err_devlist(self.ssn_list, self.job_folder)

        l = []
        with open(self.job_folder/'err.csv', newline='') as f:
            csv_reader = csv.DictReader(f)
            for row in csv_reader:
                s = Session(row["IP"], row["Name"], 'adm', 'pwd')
                s.errmsg = row["Error"]
                l.append(str(s))

        l2 = [str(ssn) for ssn in self.ssn_list]
        # print(self.ssn_list_str)

        self.assertEqual(l, l2)


    def test_process_ok_devlist(self):
        for session in self.ssn_list:
            # lines returned by IOS end with \r\n
            session.outputs = [
                {"cmd": "show ver", "output": "version 1.0"},
                {"cmd": "show interface", 
                 "output": ("Interface Name  Status\r\n"
                            "Gi1             up\r\n"
                            "Gi2             admin down\r\n"
                            "Gi3             down\r\n")
                },
            ]
        r = process_ok_devlist(self.ssn_list, self.job_folder, True)

        for s in self.ssn_list:
            fn = self.job_folder/(s.hostname + '.txt')
            self.assertTrue(fn.exists())
        self.assertTrue((self.job_folder/"output.csv").exists())
        self.assertTrue((self.job_folder/"output.json").exists())
        self.assertTrue((self.job_folder/"output.yml").exists())
        

