import unittest
from unittest.mock import patch

import sys
import errno
from types import SimpleNamespace
from tempfile import TemporaryDirectory


# Suppress the CryptographyDeprecationWarning from Paramiko
import warnings
warnings.simplefilter("ignore")

from prunlib.device import Device
from prunlib.session import Session
from prun import main, parse_cli, main_process
from process_output import process_err_devlist, process_ok_devlist

# logging.basicConfig(level=logging.DEBUG)

class TestParseCli(unittest.TestCase):
    """Test parse_cli() in the main module."""

    def test_parse_cli_1(self):
        args = parse_cli([
            '--psw', 'secret', '-t', '10', '-u', 'user1', 'csvfile', 'cmdfile', '--csv'])

        # print(args)
        self.assertEqual(args.psw, 'secret')
        self.assertEqual(args.thread, 10)
        self.assertEqual(args.user, 'user1')
        self.assertEqual(args.csvfile, 'csvfile')
        self.assertEqual(args.cmdfile, 'cmdfile')
        self.assertTrue(args.csv)
        self.assertTrue(args.log2file)
        self.assertFalse(args.debug)
        self.assertIsNone(args.output_dir)


    @patch('getpass.getuser', autospec=True)
    def test_parse_cli_2(self, mock_getuse):
        mock_getuse.return_value = 'mock_user'

        args = parse_cli(['--psw', 'secret', '-t', '10', 'csvfile', 'cmdfile'])
        self.assertEqual(args.user, 'mock_user')


class TestMainProcess(unittest.TestCase):
    """Test main_process() in the main module."""

    @patch.object(Session, 'cli_session', autospec=True)
    def test_main_process(self, mock_method_cli_session):
        """>> Test main_process() in the main module"""

        args = SimpleNamespace(user='tester', psw='testpsw', thread=1)

        dev_count = 4
        dev_list = [
            {"IP": f"10.1.1.{i}", "Name": f"router{i}"} for i in range(1,dev_count+1)
        ]

        cmd_list = [
            {'cmd':'show ver'}, {'cmd':'show int'}
        ]

        session_list = main_process(args, dev_list, cmd_list)

        # for s in session_list:
        #     print(f'<<<{s}>>>')
        self.assertEqual(len(session_list), dev_count)
        self.assertIsInstance(session_list[0], Session)


class TestMainFuction(unittest.TestCase):
    """Test main() in the main module."""

    def setUp(self):
        self.base = TemporaryDirectory(dir='tests', delete=False) # no auto-cleanup
        # print(f'job={self.base.name}')

        # --no-log: do not create log files which will prevent tmpdir cleanup
        self.cli = ['prun', 'csvfile', 'cmdfile', '-u', 'admin', '--psw', 'password', 
                    '--output-dir', self.base.name, '--no-log']

        # prevent duplicated log messages during unittest
        self.patcher_log = patch('common.setup_log')
        self.patcher_log.start()


    def tearDown(self):
        self.base.cleanup()
        self.patcher_log.stop()


    @patch('pathlib.Path.mkdir', autospec=True)
    def test_main_1_return_1(self, mock_mkdir):
        """>> Test main() returning 1 (failed to created job folder)"""

        sys.argv = self.cli
        mock_mkdir.side_effect = [OSError(errno.EACCES, 'Mocked OSError', self.base.name)]

        return_code = main()
        self.assertEqual(return_code, 1)


    @patch('prunlib.devlist.read_devices', autospec=True)
    def test_main_2_return_2(self, mock_devlist):
        """>> Test main() returning 2 (csvfile error)"""

        sys.argv = self.cli
        mock_devlist.return_value = ('Mocked devlist csv error', [])

        return_code = main()
        self.assertEqual(return_code, 2)


    @patch('prunlib.cmdlist.read_commands', autospec=True)
    @patch('prunlib.devlist.read_devices', autospec=True)
    def test_main_3_return_3(self, mock_devlist, mock_read_commands):
        """>> Test main() returning 3 (cmdfile error)"""

        sys.argv = self.cli
        mock_devlist.return_value = ('OK', [{'IP':'10.1.1.1', 'Name':'R1'}, {'IP':'10.1.1.2', 'Name':'R2'}])
        mock_read_commands.return_value = ('Mocked cmd file error', [])

        return_code = main()
        self.assertEqual(return_code, 3)


    @patch('prun.process_result', autospec=True)
    @patch('prun.main_process', autospec=True)
    @patch.object(Device, 'connect', autospec=True)
    @patch('prunlib.cmdlist.read_commands', autospec=True)
    @patch('prunlib.devlist.read_devices', autospec=True)
    def test_main_4_return_0(self, mock_devlist, mock_read_commands, mock_dev_connect, mock_main_process, mock_process_result):
        """>> Test main() returning 0 (success)"""

        sys.argv = self.cli
        mock_devlist.return_value = ('OK', [{'IP':'10.1.1.1', 'Name':'R1'}, {'IP':'10.1.1.2', 'Name':'R2'}])
        mock_read_commands.return_value = ('OK', [{'cmd':'show version'}])
        mock_dev_connect.return_value = True

        return_code = main()
        self.assertEqual(return_code, 0)
        

