import unittest
import logging
from time import sleep
from random import random
from prunlib.taskrunner import TaskRunner, Task


# logging.basicConfig(level=logging.DEBUG)

def task_worker(task):
    sleep(1 * random())
    return task


def sort_list(a_list):
    return sorted([repr(e) for e in a_list])


def create_task_list(count):
    payload_list = [
        {"IP": f"10.1.1.{i}", "Name": f"router{i}", "action": "reboot"} for i in range(count)
    ]
    task_list = [Task(p) for p in payload_list]

    # print(f'task_list={task_list}')
    return task_list, payload_list


def drive_task_runner(num_thread, num_task):
    
    task_list, payload_list = create_task_list(num_task)

    task_runner = TaskRunner(num_thread, task_worker, task_list)
    res_list = task_runner.run()
    # print(f'\n{res_list}')
    # print(f'\n{sort_list(res_list)}')
    return payload_list, res_list


class TestTaskRunner(unittest.TestCase):
    """Test TaskRunner Class """

    def test_run_1(self):
        """>> 10x threads, 1000x tasks """

        num_th, num_tasks = 10, 1000
        payload_list, res_list = drive_task_runner(num_th, num_tasks)

        self.assertEqual(len(res_list), num_tasks)
        self.assertEqual(sort_list(res_list), sort_list(payload_list))
        

    def test_run_2(self):
        """>> 10x threads, 3x tasks """

        num_th, num_tasks = 10, 3
        payload_list, res_list = drive_task_runner(num_th, num_tasks)

        self.assertEqual(len(res_list), num_tasks)
        self.assertEqual(sort_list(res_list), sort_list(payload_list))


    def test_run_3(self):
        """>> 0x threads, 3x tasks """

        num_th, num_tasks = 0, 3
        payload_list, res_list = drive_task_runner(num_th, num_tasks)

        self.assertEqual(len(res_list), num_tasks)
        self.assertEqual(sort_list(res_list), sort_list(payload_list))



