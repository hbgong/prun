import unittest
from unittest.mock import Mock

import logging
import paramiko
from prunlib.session import Session, _remove_first_line
from prunlib.device import Device

# logging.basicConfig(level=logging.DEBUG)


class TestRemoveFirstLine(unittest.TestCase):
    def test_1(self):
        """>> _remove_first_line()"""
        s = '123\n456\n789'
        self.assertEqual(_remove_first_line(s), '456\n789')

        s = '123\n'
        self.assertEqual(_remove_first_line(s), '')

        s = '123'
        self.assertEqual(_remove_first_line(s), '')


class TestSession(unittest.TestCase):
    """Test class Session """

    def setUp(self):
        self.saved = paramiko.transport.Transport
        self.m = Mock(name='MockTransport', spec=self.saved)
        paramiko.transport.Transport = self.m
        # print(f'setUp m -> {self.m}')

        # when Device.connect() is called, its ssh_transport attribute will be the same mock
        self.m.return_value = self.m
        self.ch = Mock(name='MockChannel', spec=paramiko.channel.Channel)
        # print(f'setUp ch -> {self.ch}')
        self.m.open_channel.return_value = self.ch

        # mock channel send()
        self.ch.send.side_effect = lambda arg : len(arg)

        self.s = Session('10.1.1.1', 'routername', 'admin', 'password')


    def tearDown(self):
        paramiko.transport.Transport = self.saved


    def test_cli_session_1(self):
        """>> Cli session success"""

        cmd_list = [{'cmd':'show ver'}, {'cmd':'show int'}]

        # mock channel recv()
        recv_list = [
            b"login banner\nrouter# ",          # note the extra space after '#'
            b"term len 0\nrouter#",             # disable pager
            b"show ver\nVersion 1.0\nrouter#",
            b"show int\ninterface line 1\ninterface line 2\nrouter#"
        ]
        self.ch.recv.side_effect = recv_list

        res = self.s.cli_session(cmd_list)
        # print(self.mock_channel.recv.call_args)
        self.assertTrue(res)
        self.assertEqual(self.s.errmsg, '')
        self.assertEqual(self.s.outputs[0]["output"], 'Version 1.0')
        self.assertEqual(self.s.outputs[0]["raw_output"], 'show ver\nVersion 1.0')
        self.assertEqual(self.s.outputs[1]["output"], 'interface line 1\ninterface line 2')
        self.assertEqual(self.s.outputs[1]["raw_output"], 'show int\ninterface line 1\ninterface line 2')


    def test_cli_session_2(self):
        """>> CLI session received EOF(SSH channel closed)"""

        cmd_list = [{'cmd':'show ver'}, {'cmd':'show int'}]
        # mock channel recv()
        recv_list = [
            b"login banner\nrouter# ", 
            b"show ver\nVersion 1.0\nrouter#",
            b"show int\ninterface line 1\ninterface line 2",
            b""
        ]
        self.ch.recv.side_effect = recv_list

        res = self.s.cli_session(cmd_list)

        self.assertIsInstance(res, Session)
        self.assertEqual(res, self.s)
        self.assertFalse(self.s.success)
        self.assertTrue('SSH channel closed' in self.s.errmsg)
        # print(f'\nSession.errmsg -> {self.s.errmsg}')


