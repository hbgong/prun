'''Test class Device.
py -m unittest discover -s tests -p test_dev* -v
'''

import unittest
from unittest.mock import Mock, DEFAULT

import logging
import time
import paramiko
from paramiko.ssh_exception import SSHException
import socket
from prunlib.device import Device, ExpectPattern

# logging.basicConfig(level=logging.DEBUG)

class TestDevice(unittest.TestCase):
    """Test class constructor"""

    def test_constructor(self):
        """>> Test Device.__init__()"""
        host, hostname, admin, password = ('10.1.1.1', 'routername', 'admin', 'password')
        d = Device(host, hostname, admin, password)

        self.assertEqual(d.addr, host)
        self.assertEqual(d.hostname, hostname)
        self.assertEqual(d.username, admin)
        self.assertEqual(d.password, password)


class TestDeviceConnect(unittest.TestCase):
    """Test Device.connect() method"""

    def setUp(self):
        self.saved_paramiko_transport = paramiko.transport.Transport
        self.d = Device('10.1.1.1', 'routername', 'admin', 'password')


    def tearDown(self):
        paramiko.transport.Transport = self.saved_paramiko_transport
    

    def test_connect_1(self):
        """>> No retry"""

        events = [
            DEFAULT, SSHException('mocked SSH error'), 
            socket.error('mocked socket error'),
            Exception('general paramiko expection')
        ]
        paramiko.transport.Transport = Mock(
            spec=paramiko.transport.Transport)
        paramiko.transport.Transport.side_effect = events

        res = self.d.connect()
        self.assertTrue(res)

        res = self.d.connect()
        self.assertFalse(res)
        self.assertTrue('mocked SSH error' in self.d.errmsg)

        res = self.d.connect()
        self.assertFalse(res)
        self.assertTrue('mocked socket error' in self.d.errmsg)

        res = self.d.connect()
        self.assertFalse(res)
        self.assertTrue('general paramiko expection' in self.d.errmsg)


    def test_connect_2a(self):
        """>> Retry=10, interval=1, 3rd event is success"""
        events = [
            socket.error('mocked socket error'),
            socket.error('mocked socket error'),
            DEFAULT
        ]
        m = Mock(spec=paramiko.transport.Transport)
        paramiko.transport.Transport = m
        m.side_effect = events

        res = self.d.connect(retry=10, interval=1)
        # print(m.call_args_list)
        self.assertTrue(res)
        self.assertEqual(m.call_count, 3)


    def test_connect_2b(self):
        """>> Retry=1, interval=1, 3rd event is success"""
        events = [
            socket.error('mocked socket error'),
            socket.error('mocked socket error'),
            DEFAULT
        ]
        m = Mock(spec=paramiko.transport.Transport)
        paramiko.transport.Transport = m
        m.side_effect = events

        res = self.d.connect(retry=1, interval=1)
        # print(m.call_args_list)
        self.assertFalse(res)
        self.assertEqual(m.call_count, 2)


class TestDeviceReadChannel(unittest.TestCase):
    """Test Device._read_channel() method"""

    def setUp(self):
        # set up a dummy Device for testing
        self.d = Device('10.1.1.1', 'routername', 'admin', 'password')
        self.d.ch = Mock(spec=paramiko.channel.Channel)

    def test_read_channel_1(self):
        """channel closed"""
        self.d.ch.recv.return_value = b''

        res = self.d._read_channel()
        self.assertEqual(res, 0)

    def test_read_channel_2(self):
        """bytes returned from the channel"""
        channel_bytes = b'01234abcd'
        channel_str = channel_bytes.decode(errors='ignore')
        self.d.ch.recv.return_value = channel_bytes

        res = self.d._read_channel()
        self.assertEqual(res, len(channel_bytes))
        self.assertEqual(self.d.expect_buf, channel_str)

    def test_read_channel_3(self):
        """T1 timeout, nothing from ch.recv()"""
        self.d.ch.recv.side_effect = socket.timeout

        res = self.d._read_channel()
        self.assertEqual(res, -1)


class TestDeviceExpect(unittest.TestCase):
    """Test Device.expect() method"""

    def setUp(self):
        # set up a dummy Device for testing
        self.d = Device('10.1.1.1', 'routername', 'admin', 'password')
        self.d.ch = Mock(spec=paramiko.channel.Channel)
        
    
    def test_expect_1(self):
        """>> Expect a prompt router# and get it"""

        channel_read = b'aabbccdd112233445566router#\na new line'
        self.d.ch.recv.return_value = channel_read

        res = self.d.expect(r'router#')
        self.assertTrue(res)
        self.assertEqual(self.d.expect_match, 'router#')
        self.assertEqual(self.d.expect_output, 'aabbccdd112233445566')
        self.assertEqual(self.d.expect_output_full, 'aabbccdd112233445566router#')
        self.assertEqual(self.d.expect_buf, '\na new line')
        self.assertEqual(self.d.matched_pattern_index, 0)


    def test_expect_2(self):
        """>> Bytes arrive in chunks. Expect a prompt router# and get it"""

        chunks = [b'aabbccdd1122334',b'45566rou', b'ter#\na', b' new line']
        self.d.ch.recv.side_effect = chunks

        res = self.d.expect(r'router#')
        self.assertTrue(res)
        self.assertEqual(self.d.expect_match, 'router#')
        self.assertEqual(self.d.expect_output, 'aabbccdd112233445566')
        self.assertEqual(self.d.expect_output_full, 'aabbccdd112233445566router#')
        self.assertEqual(self.d.expect_buf, '\na')
        self.assertEqual(self.d.matched_pattern_index, 0)


    def test_expect_3(self):
        """>> Bytes arrive in chunks. Expect prompts or EOF, get an EOF"""

        chunks = [b'aabbccdd1122334',b'45566rou', b'ter>\na', b' new line', b'']
        self.d.ch.recv.side_effect = chunks

        res = self.d.expect(['router#', ExpectPattern.EOF, 'switch>'])
        self.assertTrue(res)
        self.assertEqual(self.d.expect_match, 'aabbccdd112233445566router>\na new line')
        self.assertEqual(self.d.expect_output, 'aabbccdd112233445566router>\na new line')
        self.assertEqual(self.d.expect_output, self.d.expect_output_full)
        self.assertEqual(self.d.expect_buf, '')
        self.assertEqual(self.d.matched_pattern_index, 1)


    def test_expect_4(self):
        """>> Bytes arrive in chunks. Expect prompts but get an EOF. Raise EOFError"""

        chunks = [b'aabbccdd1122334',b'45566rou', b'ter>\na', b' new line', b'']
        self.d.ch.recv.side_effect = chunks

        with self.assertRaises(EOFError):
            self.d.expect(['router#', 'switch>'])


    def test_expect_5(self):
        """>> No match until timeout(2s), raise Timeouterror"""

        def func_side_effect(*args, **kwargs):
            time.sleep(0.5)
            return b'a test line '
       
        self.d.ch.recv.side_effect = func_side_effect

        with self.assertRaises(TimeoutError):
            self.d.expect(['router#', 'switch>'], timeout=2 )


    def test_expect_6(self):
        """>> No match, but a timeout(3s) is expected"""

        def func_side_effect(*args, **kwargs):
            time.sleep(0.5)
            return b'a test line '
        
        self.d.ch.recv.side_effect = func_side_effect

        res = self.d.expect(['router#', 'switch>', ExpectPattern.TIMEOUT], timeout=3 )
        self.assertTrue(res)
        self.assertEqual(self.d.expect_match, '')
        self.assertEqual(self.d.expect_output, '')
        self.assertEqual(self.d.expect_output, self.d.expect_output_full)
        self.assertTrue(self.d.expect_buf.startswith('a test line '))
        self.assertEqual(self.d.matched_pattern_index, 2)


class TestDeviceSend(unittest.TestCase):
    """Test Device.send() method"""

    def setUp(self):
        # set up a dummy Device for testing
        self.d = Device('10.1.1.1', 'routername', 'admin', 'password')
        self.d.ch = Mock(spec=paramiko.channel.Channel)
        
    
    def test_send_1(self):
        """>> Send a string without LF in one go"""
        test_string = 'a123456789b123456789'
        self.d.ch.send.return_value = len(bytes(test_string, 'utf-8')) + 1
        res = self.d.send(test_string)
        self.assertTrue(res)


    def test_send_1a(self):
        """>> Send a string with LF in one go"""
        test_string = 'a123456789b123456789\n'
        self.d.ch.send.return_value = len(bytes(test_string, 'utf-8'))
        res = self.d.send(test_string)
        self.assertTrue(res)


    def test_send_2(self):
        """>> Send a string in 3 times"""
        test_string = '00000111112222233333'
        counts = [9, 8, 4]
        self.d.ch.send.side_effect = counts
        res = self.d.send(test_string)
        # print(f'\nch.send():{self.d.ch.send.call_args_list}')
        self.assertTrue(res)


    def test_send_2a(self):
        """>> Send a string in 3 times"""
        test_string = '00000111112222233333\n'
        counts = [9, 8, 4]
        self.d.ch.send.side_effect = counts
        res = self.d.send(test_string)
        # print(f'\nch.send():{self.d.ch.send.call_args_list}')
        self.assertTrue(res)
