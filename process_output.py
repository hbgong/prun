"""
This module processes outputs received from the target devices.
"""

import logging
import csv
import argparse
import json
import textwrap
from pathlib import Path
from prunlib.session import Session

logger = logging.getLogger("process_output") # my top level logger

def process_err_devlist(err_dev_list: list, job_folder: Path) -> str:
    try:
        with open(job_folder/"err.csv", 'w', newline='') as csvfile:
            csv_writer = csv.writer(csvfile)
            csv_writer.writerow(["Name", "IP", "Error"])
            for session in err_dev_list:
                csv_writer.writerow([session.hostname, session.addr, session.errmsg])
    except OSError as err:
        return f"{err}"
    return ''


def  process_ok_devlist(ok_dev_list: list, job_folder: Path, generate_csv: bool) -> str:
    """Save output from each device into a seperate file under the output folder.
       Also save into output.csv in the same folder.
    
       In each Session object, there is a list of device output created as follows:
            self.outputs.append(
                {
                    "cmd": cmd["cmd"], 
                    "raw_output": self.expect_output,
                    "output": _remove_first_line(self.expect_output)
                }
            )
    """
    try:
        for session in ok_dev_list:
            fn = job_folder / (session.hostname + ".txt")
            with open(fn, 'w') as of: # '\n' will be translated to system default line seperator
                for o in session.outputs:
                    output = f"\n{80*'*'}\n{o['cmd']}\n{80*'*'}\n{o['output']}\n"
                    logger.debug(f"[- {output} -]\n")
                    for line in output.splitlines():
                        of.write(line + '\n')

        if generate_csv:
            with open(job_folder/"output.csv", 'w', newline='') as csvfile:
                csv_writer = csv.writer(csvfile)
                csv_writer.writerow(["Name", "Command", "Output"])
                for session in ok_dev_list:
                    for o in session.outputs:
                        csv_writer.writerow([session.hostname, o["cmd"], o["output"]])

        # alway generate a json output file
        obj_list = []
        with open(job_folder/"output.json", 'w') as jsfile:
            for session in ok_dev_list:
                js_obj = {"Name": session.hostname, "Outputs": []}
                for o in session.outputs:
                    js_obj["Outputs"].append(
                        {"Command":o["cmd"], "Output":o["output"]} 
                    )
                obj_list.append(js_obj)
            json.dump(obj_list, jsfile, indent=4)

        # Generate a YAML output file
        yaml_1 = '''\
- Name: {}
  Outputs:
'''
        yaml_2 = '''\
  - Command: {}
    Output: |+
'''
        s = ""
        with open(job_folder/"output.yml", 'w') as ymlfile:
            for session in ok_dev_list:
                s += yaml_1.format(session.hostname)
                for o in session.outputs:
                    s += yaml_2.format(o["cmd"])
                    s += textwrap.indent(o["output"], 6*' ')
            for line in s.splitlines():
                ymlfile.write(line + '\n')
        
    except OSError as err:
        return f"Error writing output file.\n{err}"
    return ''


def process_result(res_list: list[Session], args: argparse.Namespace, job_folder: Path):
    logger.info('\n\nSUMMARY\n' + '*'*80)

    # list of devices with error
    err_dev_list = [session for session in res_list if not session.success]
    # list of device without error
    ok_dev_list = [session for session in res_list if session.success]

    # display summary
    if ok_dev_list:
        logger.info(("\nNumber of devices with task completed: %d\n" + "="*80) % len(ok_dev_list))
    if err_dev_list:
        logger.info(("\nNumber of devices with error: %d\n" + "="*80) % len(err_dev_list))
        for d in err_dev_list:
            logger.info(f'{d.addr}({d.hostname}) {d.errmsg!r}')

    if ok_dev_list:
        res = process_ok_devlist(ok_dev_list, job_folder, args.csv)
        if res != '':
            logger.info(f'\n{res}')
    if err_dev_list:
        res = process_err_devlist(err_dev_list, job_folder)
        if res != '':
            logger.info(f'\n{res}')

