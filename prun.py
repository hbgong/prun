"""
Run a command sequence through SSH CLI of multiple network devices in parallel

It works in this way:
    1. read a list of devices from a csv file
    2. read a list of commands from a csv or json file
    3. save output from each device into a seperate file under a job folder
"""
# Suppress the CryptographyDeprecationWarning from Paramiko
import warnings
warnings.simplefilter("ignore")

import argparse
import getpass
import logging
import sys
import time
from datetime import timedelta, datetime
from pathlib import Path

import common
from prunlib import devlist, cmdlist
from prunlib.taskrunner import TaskRunner, Task
from prunlib.session import Session
from process_output import process_result

logger = logging.getLogger("prunlib") # my top level logger


def parse_cli(args=None):
    """Parse command line arguments and options"""
    
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="Run commands on multiple network devices.        "
                    "Version " + common.version)

    # required positional arguments
    #
    parser.add_argument("csvfile", help="CSV file with target devices", 
                        metavar="TARGETS")
    parser.add_argument("cmdfile", help="CSV or JSON file with commands to run",
                        metavar="CMDFILE")

    # optional arguments
    #
    parser.add_argument(
        "--user", "-u", default=getpass.getuser(), metavar="USER",
        help="Username for device login")

    parser.add_argument(
        "--psw", nargs='?' , default='', const='', metavar="PASSWORD",
        help="Password for device login")

    parser.add_argument(
        "--thread", "-t", nargs='?' , type=int, 
        default=1, const=2, metavar="NUMBER",
        help="Number of threads (default:2) used to access devices")

    # Options specifying output file formats
    parser.add_argument(
        "--output-dir", metavar="DIR",
        help="Directory for storing output files. Default:job-mmdd-hhmmss")

    parser.add_argument(
        "--csv", action="store_true",
        help="Save results to a csv file")

    parser.add_argument(
        "--no-log", action="store_false", dest='log2file',
        help="Do not write output to a log file")
        
    parser.add_argument("--debug", action="store_true", help="Turn on debug logging")
   
    return parser.parse_args(args)


def main_process(args, dev_list, cmd_list) -> list[Session]:

    def task_worker(task: dict) -> Session:
        s = Session(task["addr"], task["hostname"], task["username"], task["password"])
        s.cli_session(task["cmd_list"])
        logger.debug(f"Session.outputs: {s.outputs}")
        return s

    task_list = []
    for d in dev_list:
        payload = {
            "addr": d["IP"],
            "hostname": d["Name"],
            "username": args.user,
            "password": args.psw,
            "cmd_list": cmd_list
        }
        tk = Task(payload)
        tk.start_lable = f'Started: {payload["hostname"]}'
        tk.end_label = f'Finished: {payload["hostname"]}'
        task_list.append(tk)

    task_runner = TaskRunner(args.thread, task_worker, task_list)
    
    try:
        return task_runner.run() 
    except KeyboardInterrupt:   
        logger.error("Aborted at user request")
        sys.exit(1)



def main():
    args = parse_cli()

    # Create job folder
    #
    if args.output_dir is None:
        args.output_dir = 'job-' + datetime.now().strftime('%d%H%M%S')
    folder = Path(args.output_dir)

    try:
        folder.mkdir(parents=True, exist_ok=True)
    except OSError as err:
        print(f"Failed to create output folder [{args.output_dir}]\n{err}")
        return 1


    # Setup logging - if threading is required, log thread name too
    #
    common.setup_log(
        logger, 
        folder / "log.log", 
        args.debug, 
        args.thread > 1, 
        args.log2file)
    logger.debug(args)              # argparse result for debugging purpose


    # Process device file
    #
    err_msg, dev_list = devlist.read_devices(args.csvfile)
    if err_msg != "OK":
        logger.error(f'Abort due to error with csv file. [{err_msg}]')
        return 2
    total = len(dev_list)


    # Process command file
    #
    err_msg, cmd_list = cmdlist.read_commands(args.cmdfile)
    if err_msg != "OK":
         logger.error(f'Abort due to error with cmd file. [{err_msg}]')
         return 3


    # ask for password if not provided on the command line
    #
    if args.psw == '':           
        try:
            args.psw = getpass.getpass("Password:")
        except KeyboardInterrupt:   
            logger.error("\nAborted at user request")
            return 4


    # Abort if password is not correct
    #
    test_dev = dev_list[0]
    test_session = Session(test_dev["IP"], test_dev["Name"], args.user, args.psw)
    if not test_session.connect():
        logger.error(f"Please check username and password are correct.\n{test_session.errmsg}")
        return 5
    else:
        test_session.close()
        del test_session


    # Print task summary
    #
    logger.info("Number of devices to be processed: %d" % total)
    logger.info("Number of threads to use: %d" % args.thread)
    logger.info("Number of commands to be executed on each device: %d" % len(cmd_list))
    logger.info(
        "Commands to be executed:\n%s\n" % ('\n'.join([str(cmd) for cmd in cmd_list]))
    )


    # Now start to work
    #
    t_start = time.time()
    res_list = main_process(args, dev_list, cmd_list)
    t_stop = time.time()


    # Process work result
    #
    process_result(res_list, args, folder)

    # Finally total time spent
    logger.info("\nTask started at: %s" % time.strftime("%x %X", time.localtime(t_start)))
    logger.info("Task stoped at:  %s" % time.strftime("%x %X", time.localtime(t_stop)))
    t_total = str(timedelta(seconds=(t_stop - t_start)))
    logger.info("Total running time: %s" % t_total)
    return 0


if __name__ == '__main__':
    sys.exit(main())