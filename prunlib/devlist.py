import csv
import re
import logging


logger = logging.getLogger("prun.devlist")	# my module level logger

def read_devices(csvfile):
    """Reads a list of target devices from the specified .csv file.

    The csv file contains an 'IP' column and an optional 'Name' column. The 
    'IP' column contains IPv4 address of the target devices. When a invalid 
    IPv4 address is encountered, the function will abort and return with an 
    error message. If the 'Name' column is not provided or its content is 
    empty, the IPv4 address will be used in its place.
    
    Args:
        csvfile: csv file containing target devices

    Returns:
        A tuple: (err_msg, dev_list)
        err_msg: set to "OK" when success, otherwise contains error message 
                 about reading the csv file
        dev_list: a list of {"IP": 10.1.1.1, "Name": "devname"}
    """

    err_msg = "OK"
    dev_list = []
    try:
        with open(csvfile, newline='') as f:
            reader = csv.DictReader(f)
            count = 0
            for csv_row in reader:
                logger.debug(csv_row)
                count += 1
                row = {}

                # Validate IP address format
                ip = csv_row['IP'].strip()
                # if re.match(r'\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}$', ip) is None:
                #     err_msg = 'Row [%d]:Invalid IP address: %s' % (count, ip)
                #     break
                row['IP'] = ip

                # if hostname is not provided, then use ip address instead
                hostname = csv_row.get('Name', ip)
                if hostname == '':
                    hostname = ip
                row['Name'] = hostname

                # This row is in correct format now
                dev_list.append(row)
    except csv.Error as err:
        err_msg = 'Format error: %s' % err
    except KeyError as err:
        err_msg = 'Key error due to format: %s' % err
    except (OSError, IOError) as err:
        err_msg = str(err)

    # Here all rows are processed and in valide format     
    logger.debug(repr(dev_list))  
    return (err_msg, dev_list)