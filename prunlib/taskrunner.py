"""Uses multi-threading to process multiple devices in parallel """

import logging
import threading
import queue
import time
from collections.abc import Callable
from typing import Any


logger = logging.getLogger(__name__) # my top level logger

class Task():

    def __init__(self, task: Any, start_lable='', end_label=''):
        self.id = id(self)
        self.start_lable = start_lable
        self.end_label = end_label
        self.payload = task

    def __repr__(self):
        return f'{type(self).__name__}({self.__dict__})'


class TaskRunner():
    '''Run multiple tasks using threads'''

    def __init__(self, num_threads: int, task_worker: Callable[[any], any], task_list: list[Task]):
        '''
        num_threads: number of threads to run
        task_worker: a callable for each thread to run. Its single argument is from the task_q, and what it returns
                     will be put into the output_q 
        task_list:   tasks to be put into the task_q
        '''
        self.num_threads = 1 if num_threads < 1 else num_threads
        self.task_worker = task_worker      
        self.task_list = task_list          

        self.task_q = queue.SimpleQueue()
        self.output_q = queue.SimpleQueue()

        # add tasks into the task queue
        for task in self.task_list:
            self.task_q.put(task)
        # None indicates no more tasks, and the worker thread should exit
        for i in range(self.num_threads):
            self.task_q.put(None)


    def __thread_worker(self) -> None:
        """Get tasks from task_q and put results back to output_q. 
        
        self.task_worker() does the actual work. It takes the objects from task_q as arguments and 
        returns result objects for putting into output_q.
        """

        th = threading.current_thread()
        logger.debug(f"<{th.name}>:Started ...")

        while True:
            task = self.task_q.get()
            if task is None:     # no more tasks in the queue
                logger.debug(f"<{th.name}>: Quit as per order")
                return

            logger.debug(f"<{th.name}>:Start processing task {task}")
            if task.start_lable:
                logger.info(f"{task.start_lable}")
            res = self.task_worker(task.payload)    # run task_worker
            if task.end_label:
                logger.info(f"{task.end_label}")

            # put result into output queue
            logger.debug(f'<{th.name}>:Thread worker returns: {res}')
            self.output_q.put(res) 


    def run(self) -> list:
        """Creates threads to run tasks. Returns objects from output_q as a list"""

        for i in range(self.num_threads):
            t = threading.Thread(target=self.__thread_worker)
            t.start()

        logger.debug("All threads started. Waiting for them to finish running ...")

        main_thread = threading.main_thread()
        active_threads = threading.enumerate()
        logger.debug(active_threads)
        for th in active_threads:
            if th is main_thread or not th.is_alive():
                logger.debug(f"<{th.name}>: Main_thread or not alive")
                continue
            logger.debug(f"<{th.name}>: Waiting for it to finish running ...")
            th.join()
            logger.debug(f"<{th.name}>: Thread finished running")

        # Now all the threads have finished running, check the output queue
        res_list = []
        while True: 
            try:
                e = self.output_q.get_nowait()
                logger.debug(f'From output Queue: {e}')
                res_list.append(e)
            except queue.Empty:
                logger.debug("Queue empty")
                break 

        return res_list


