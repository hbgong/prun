"""Class for accessing network device CLI via SSH, with expect/send syntax"""

import logging
import re
import socket
import time
# from random import randrange # for testing Exception in connect() method
from enum import Enum

import paramiko
from paramiko.ssh_exception import SSHException

logger = logging.getLogger(__name__)  # my module level logger

class ExpectPattern(Enum):
    EOF = 1
    TIMEOUT = 2


class Device():
    """Abstraction of a network device supporting SSH access to CLI"""

    def __init__(self, addr: str, hostname: str, user: str, pwd: str):
        """Initializes the Device object.

        addr: address of the network device
        hostname: device name
        user: username used for accessing the device
        pwd: pasword used for accessing the device
        """

        self.addr = addr
        self.hostname = hostname
        self.username, self.password = user, pwd
        self.errmsg = ''

        self.ssh_transport = None
        self.ch = None              # paramiko SSH channel
        self.T1 = 0.3               # seconds waiting for the next chunk of bytes coming from the SSH channel
        self.BLOCKSZ = 8096

        self.expect_buf = ''		# buffer in which the expected patterns are searched for
        self.re_match = None        # re.Match object when a pattern is matched
        self.expect_match = ''      # the string that matches a pattern
        self.expect_output = ''         # from the start of expect_buf up to where the match starts
        self.expect_output_full = ''    # from the start of expect_buf up to the end of the match
        self.matched_pattern_index = -1 # indictes which pattern has matched
        

    def connect(self, retry=0, interval=10):
        """Creates a SSH channel to the network device. SSH negotiation and 
        authentication wil be performed during the course.

        Connect retrying can be useful when waiting for a device to reboot.

        Returns True when the SSH channel is established, False otherwise.
        """
        while True:
            try:
                self.ssh_transport = paramiko.transport.Transport(self.addr)
                self.ssh_transport.connect(username=self.username, password=self.password)
                self.ch = self.ssh_transport.open_channel("session")
                self.ch.get_pty()
                self.ch.invoke_shell()
                return True
            
            except (SSHException, socket.error) as err:
                self.errmsg = f"{err}"
                logger.debug(f'SSHException, socket.error -> {self.errmsg}')
                self.close()
                if retry > 0:
                    logger.debug(f'Retries left: {retry}')
                    time.sleep(interval)
                    retry -= 1
                    continue
                else:
                    logger.debug("SSHException, socket.error -> return False")
                    return False

            except Exception as err:
                self.errmsg = f"Unexpected paramiko library exception. More details:\n{err}"
                logger.debug(f"Unexpected exception: {err}")
                return False
        

    def close(self):
        """Closes the SSH connection to the network device."""

        # Only close a Transport obj when it has been created. 
        # Channels associated with it will be closed automatically
        try:
            if self.ssh_transport:
                self.ssh_transport.close() 

        except Exception as err:
            logger.debug(f"Unexpected exception while closing ssh_transport: {err}")
            # do nothing


    def _read_channel(self):
        """Reads bytes from the SSH channel. Called by self.expect().

        * returns 0 if channel is closed (EOF)
        * returns -1 if nothing available during the T1 period
        * returns len(s) otherwise. The string is appended to self.expect_buf 
        """
        self.ch.settimeout(self.T1)     # set a timeout on blocking read from SSH channel

        try:
            nbytes = self.ch.recv(self.BLOCKSZ)
            if len(nbytes) == 0:
                logger.debug("channel closed")
                return 0
            else:
                logger.debug("from channel: [%s]" % repr(nbytes))
                s = nbytes.decode(errors='ignore')   # decode bytes to str
                self.expect_buf += s
                return len(s)
        except socket.timeout:
            logger.debug("T1 timeout, nothing from ch.recv()")
            return -1


    def expect(self, patterns, timeout=10):
        """Searches for patterns in the SSH channel. 

        'patterns' is a list. Acceptable patterns inlucde: RE string, EOF, TIMEOUT.
        If EOF or timeout happens but is not expected, raises EOFError or
        TimeoutError accordingly.

        Returns True when a match is found, False otherwise.
        """

        def match_patterns(patterns):
            """Tries to match the patterns against the contents in self.expect_buf.

            Returns True when a match is found. 
            """
            for index, pat in enumerate(patterns):
                # Only process RE patterns, ignores the EOF/TIMEOUT pattern
                if isinstance(pat, str):
                    re_match = re.search(pat, self.expect_buf)
                    if re_match: 
                        # caller can access this re.Match object for more info
                        self.re_match = re_match    
                        self.expect_match = re_match.group(0)   # the entire match
                        self.expect_output = self.expect_buf[:re_match.start()]
                        self.expect_output_full = self.expect_buf[:re_match.end()]
                        self.expect_buf = self.expect_buf[re_match.end():]
                        self.matched_pattern_index = index
                        return True
            return False


        # a single pattern does not have to be provided as a list
        if not isinstance(patterns, list):
            patterns = [patterns]

        start_time = time.time()
        end_time = start_time + timeout
        match_found = False
        while time.time() <= end_time and not match_found:
            res = self._read_channel()
            if res == -1:   # nothing on the channel
                continue
            
            if res == 0:  # EOF is encountered
                if ExpectPattern.EOF in patterns:
                    match_found = True
                    self.re_match = None        # No re.Match object for the caller
                    self.expect_output = self.expect_buf    # move everything in the buffer to output
                    self.expect_output_full = self.expect_buf
                    self.expect_match = self.expect_output  # all is considered a match
                    self.expect_buf = ''        # clear the buffer
                    self.matched_pattern_index = patterns.index(ExpectPattern.EOF)
                    break
                else:
                    raise EOFError("SSH channel closed")

            # now we have some characters for the patters to match
            logger.debug(f"expect_buf({len(self.expect_buf)}) -> {self.expect_buf!r}")
            match_found = match_patterns(patterns)
            logger.debug(f"{'MATCH' if match_found else 'NO MATCH'} -> [{patterns}]")

        # here we have either a match, or a timeout
        if match_found:
            logger.debug(f'MATCH self.expect_output_full({len(self.expect_output_full)}) -> {self.expect_output_full}')
            return True
        else:   # we have a timeout
            if ExpectPattern.TIMEOUT in patterns:
                self.matched_pattern_index = patterns.index(ExpectPattern.TIMEOUT)
                return True
            else:
                raise TimeoutError(f"expect({patterns}) timeout")
        
        
    def send(self, txt_string, no_lf=False):
        """Send a text string to the SSH channel.
        Append a \n if the string is not ended with it by default.
        
        Returns bool to indicate if the sending is successful or not.
        """
        if not no_lf and not txt_string.endswith('\n'):
            txt_string += '\n'
        byte_string = bytes(txt_string, 'utf-8')
        len_bytes = len(byte_string)
        try:
            while True:
                n = self.ch.send(byte_string)
                if n == 0:  # channel closed
                    return False
                if n == len_bytes: 
                    return True
                byte_string = byte_string[n:]
                len_bytes = len(byte_string)
            
        except socket.timeout as err:
            logger.error("timeout in ch.send()")
            self.errmsg = f'Error:{err}\nText:{txt_string}'
            return False


 
