"""Load command list from files of various formats (csv and json).

Structure of the a command:
{
    "cmd": "show ver",
    "expect": "Version 9.0",
    t1: 10,
    t2: 0.1,
    "ignore": boolean  // the cmd will be ignored from the returned list when True
}
"""

import csv
import logging
import json


logger = logging.getLogger("prun.cmdlist")	# my module level logger

def _read_commands_json(cmdfile):
    """Reads a command list from a json file.
    
    Args:
        cmdfile: json file containing commands

    Returns:
        A tuple: (err_msg, cmd_list). 
        err_msg: set to "OK" when success, otherwise contains error message about the json file
        cmd_list: list of command structure from the json file
    """

    err_msg = "OK"
    cmd_list = []
    try:
        with open(cmdfile) as f:
            raw_cmd_list = json.load(f)
            logger.debug("json.load: " + repr(raw_cmd_list))
    except json.JSONDecodeError as err:
        err_msg = 'Format error: %s' % err
    except (OSError, IOError) as err:
        err_msg = str(err)
    else:
        cmd_list = [cmd for cmd in raw_cmd_list if not cmd.get("ignore", False) ]

    return (err_msg, cmd_list)


def _read_commands_csv(csvfile):
    """Reads a command list from a csv file.

    Each row in the csv file represents a command. Its columns contain fields
    of the command structure

    Args:
        csvfile: csv file containing commands

    Returns:
        A tuple: (err_msg, cmd_list).
        err_msg: set to "OK" when success, otherwise contains error message
                 about the json file
        cmd_list: list of dictionary (command structure)
    """

    err_msg = "OK"
    cmd_list = []

    try:
        with open(csvfile, newline='') as f:
            reader = csv.DictReader(f)
            for csv_row in reader:
                logger.debug(csv_row)

                if csv_row.get("ignore", "").upper() in ("YES", "TRUE"):
                    continue

                # if 'cmd' column is empty, ignore the row.
                # if 'cmd' column doesn't exist, KeyError will be triggered
                if csv_row['cmd'] == '':
                    continue

                cmd_list.append(dict(csv_row))  # from OrderedDict to dict
    except csv.Error as err:
        err_msg = 'Format error on line %d: %s' % (reader.line_num, err)
    except KeyError as err:
        err_msg = 'Bad Format on line %d caused key error [%s]' % (reader.line_num, err)
    except (OSError, IOError) as err:
        err_msg = str(err)

    return (err_msg, cmd_list)


def read_commands(cmdfile):
    """Read a list of commands from the given file.

    Args:
        cmdfile: a csv or json file to be processed

    Returns:
        A tuple: (err_msg, cmd_list).
        err_msg: set to "OK" when success, otherwise contains error message
                 about the json file
        cmd_list: list of dictionary
    """

    f = cmdfile.lower()
    if f.endswith('.csv'):
        return _read_commands_csv(f)
    elif f.endswith('.json'):
        return _read_commands_json(f)
    else:
        return("Unknown file type. Must be a .csv or .json file", [])