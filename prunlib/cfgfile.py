"""Load configuration settings from a json file

At the moment, used to load t1/t2/t3 custom settings
"""

import logging
import json

logger = logging.getLogger("prun.cfgfile")	# my module level logger

def read_config(cfgfile):
    """Read configuration settings from the json file.

    Args:
        cfgfile: a json file with configuration settings

    Returns:
        A tuple: (err_msg, dict_conf).
        err_msg: set to "OK" when success, otherwise contains error message
                 about the json file
        dict_conf: dictionary of settings
    """

    err_msg = "OK"
    dict_conf = {}

    try:
        with open(cfgfile) as f:
            dict_conf = json.load(f)
            logger.debug("json.load: " + repr(dict_conf))
    except json.JSONDecodeError as err:
        err_msg = 'Format error: %s' % err
    except (OSError, IOError) as err:
        err_msg = str(err)

    return (err_msg, dict_conf)

