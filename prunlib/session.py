"""Class supporting a CLI session

A CLI session starts from making a SSH connection to a device, ends when the command list
has been executed and the SSH connection closed.
"""

import logging
from prunlib.device import Device

logger = logging.getLogger(__name__)  # my module level logger

def _remove_first_line(s):
    return s.partition('\n')[2]

class Session(Device):
    """Abstraction of a CLI session to a device"""

    def __init__(self, *args, **kwargs):
        """Initializes a session.
        
        Device IP and credentials will be passed over to the base class.
        """
        # self.prompt = r'\n.+?[>#$]\s*?$'
        self.exec_prompt = r'\n.+?[>$]\s*?$'
        self.priv_prompt = r'\n.+?#\s*?$'
        self.prompt_timeout = 20            # time to wait for prompt. should be configurable
        self.pager_off_cmd = "term len 0"

        self.outputs = []
        self.success = False
        
        super().__init__(*args, **kwargs)


    def __str__(self):
        return str(self.__dict__)
    

    def _expect(self, patterns):
        return self.expect(patterns, timeout=self.prompt_timeout)


    def cli_session(self, cmd_list: list):
        """Starts a CLI session and run a list of command.

        cmd_list: A list of dictionary. Each dictionary respresents a command to be executed
        Return: all return status are recorded in the Session class itself
        """
        # Open SSH connection to the device
        if not self.connect():
            # Device.errmsg has the details
            logger.info(f'Connection attempt failed.\n{self.errmsg}')
            self.success = False
            return self

        try: 
            # expect for the first prompt afer login
            self._expect([self.exec_prompt, self.priv_prompt])
            
            # enter enable mode if needed
            if self.matched_pattern_index == 0:
                self.send('enable')
                self._expect(r'\n[Pp]assword:\s*?$')
                self.send(self.password)
                self._expect(self.priv_prompt)

            # turn off pager
            self.send(self.pager_off_cmd)
            self._expect(self.priv_prompt)
                
            for cmd in cmd_list:
                logger.debug(f"send_cmd: [{cmd}]")
                self.send(cmd["cmd"])
                self._expect(self.priv_prompt)
                self.outputs.append(
                    {
                        "cmd": cmd["cmd"], 
                        "raw_output": self.expect_output,
                        "output": _remove_first_line(self.expect_output)
                    }
                )
            self.close()
            self.success = True
            logger.debug(f"self.outputs: {self.outputs}")
            return self
        
        except (EOFError, TimeoutError) as err:
            self.close()
            self.errmsg = str(err)
            self.success = False
            logger.debug(f"EOF/Timeout during cli session: [{err}]")
            return self
            
