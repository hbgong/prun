"""
This modules contains common functions
"""

import logging
import re
import sys
import time
from itertools import zip_longest

version = "3.1.0"

def time2str(tm):
    """Convert time epoch to a string representing local time"""
    return time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(tm))


def sec2time(sec):
    """Convert seconds to hour:min:sec"""
    s = "%02d:" % (sec // 3600) if sec // 3600 != 0 else "00:"
    sec = sec % 3600
    s += "%02d:" % (sec // 60) if sec // 60 != 0 else "00:"
    s += "%02d" % (sec % 60)
    return s


def setup_log(logger, logfile, debug_flag=False, thread_flag=False, filelog_flag=False):
    """Sets up logging facilities.

    Logs message to console. Also logs to file as specified or when debug flag is set. 
    """

    logger.setLevel(logging.DEBUG)

    # log to console
    logConsoleFormatter = logging.Formatter('%(message)s')
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(logConsoleFormatter)
    ch.setLevel(logging.INFO)
    logger.addHandler(ch)

    if filelog_flag or debug_flag:
        fh = logging.FileHandler(logfile, mode="w")
        logger.addHandler(fh)

    if filelog_flag:
        fh.setFormatter(logConsoleFormatter)
        fh.setLevel(logging.INFO)

    if debug_flag:
        if thread_flag:
            f = ('%(asctime)s %(name)s %(threadName)s '
                 '[%(funcName)s.%(levelname)s] %(message)s')
        else:
            f = '%(asctime)s %(name)s [%(funcName)s.%(levelname)s] %(message)s'
        logFileFormatter = logging.Formatter(fmt=f, datefmt='%H:%M:%S')
        fh.setFormatter(logFileFormatter)
        fh.setLevel(logging.DEBUG)


def add_prefix_to_lines(lines, prefix):
    """Add prefix str to every line, and enclose with braces

    Args:
        lines: a string of multiple lines
        prefix: a string to add to the start of each line

    Returns:
        a string
    """

    s = prefix + "{\n"
    s += '\n'.join([prefix + l for l in lines.splitlines()])
    s += '\n' + prefix + "}"

    return s


def line_filter(lines, pattern):
    """Returns lines that match the pattern"""

    return '\n'.join(
        [l for l in lines.splitlines() if re.search(pattern, l)]
    )


def add_time_to_filename(basename):
    """ Insert current time HHMMSS into a filename"""

    fn = list(basename.partition('.'))
    tm = time.strftime("-%H%M%S", time.localtime())

    fn[0] += tm
    return ''.join(fn)


def str2columns(str1, str2, width1=0, width2=0):
    """Put strings into columns. String can be of multiple lines"""

    list1 = str1.split('\n')                                    # ['11', '22']
    list2 = str2.split('\n')                                    # ['aa', 'bb', 'cc']
    combo_list=list(zip_longest(list1, list2, fillvalue=''))    # [('11,'aa'), ('22', 'bb'), ('', 'cc')]

    if width1 == 0:
        width1 = max([len(e) for e in list1]) +1    # length of the longest string
    if width2 == 0:
        width2 = max([len(e) for e in list2])
    fmtstr = f"{{:<{width1}}}{{:<{width2}}}"
    fmt_list = [fmtstr.format(item[0], item[1]) for item in combo_list]
    return '\n'.join(fmt_list)