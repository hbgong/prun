import subprocess
import common

ver = common.version.replace('.', '')
cmd = f'pyinstaller -F -n prun{ver} prun.py'

subprocess.run(cmd.split())

